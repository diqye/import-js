mod(function(){
  function fjs(path){
    return function(){
      return imptjs(path)
    }
  }
  imptjs('./a.js')
  .then(function(a){
    document.body.innerHTML += JSON.stringify(a)
  })
  .then(fjs('./b.js'))
  .then(function(xs){
    document.body.innerHTML += JSON.stringify(xs)
  })
})